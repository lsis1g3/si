//Definir Pinos

//Motores:
#define pwm_d  3 //PWM Motor A - Direita
#define pwm_e  11 //PWM Motor B - Esquerda
#define dir_d  12 //DIR Motor A - Direita
#define dir_e  13 //DIR Motor B - Esquerda


//Chama:
#define PinChama  2
#define sensorValue  A4
int ventoinha = 0;
    
//Sonares:
int trigPinF=7;
int echoPinF=6;
int trigPinD=5;
int echoPinD=4;
int trigPinE=8;
int echoPinE=9;

double tempo;

int flag=0;

long espacoEsq;
long espacoDir;

void setup(){    
 //Sonares
   pinMode(trigPinF, OUTPUT);
   pinMode(echoPinF, INPUT); 
   pinMode(trigPinD, OUTPUT);
   pinMode(echoPinD, INPUT);
   pinMode(trigPinE, OUTPUT);
   pinMode(echoPinE, INPUT);
   Serial.begin(9600);   
    
 //Motores
   pinMode(pwm_d, OUTPUT);
   pinMode(pwm_e, OUTPUT);
   pinMode(dir_d, OUTPUT);
   pinMode(dir_e, OUTPUT);
  
 //Chama:
   pinMode (PinChama,OUTPUT);  
}    
    


 
// Função para ler e calcular a distância do sonar  
long lerSonar(int trigPin, int echoPin){
  long distancia;  

digitalWrite(trigPin, LOW);
delayMicroseconds(2);
digitalWrite(trigPin, HIGH);
delayMicroseconds(10);
digitalWrite(trigPin, LOW);
tempo = pulseIn(echoPin, HIGH);
distancia = (tempo/2) / 29.1;
  
  return distancia;                                 // Retorna a distância  
}   

   
// Função para determinar a distância da frente   
long DistanciaFrente(){   
  return (lerSonar(trigPinF,echoPinF));   
}    
    
// Função para determinar a distância da direita    
long DistanciaDireita(){    
  return (lerSonar(trigPinD,echoPinD));   
}    
    
// Função para determinar a distância da esquerda    
long DistanciaEsquerda(){      
  return (lerSonar(trigPinE,echoPinE));   
}    
//Função responsável pelo movimento do robô
//Chama outras funções


 
void set_velocidade(int left, int right)
{
  if(left<0)
  {
    left=abs(left);
    digitalWrite(dir_e, LOW);
    analogWrite(pwm_e, left);    
  }
  else
  {
    digitalWrite(dir_e, HIGH);
    analogWrite(pwm_e, left);    
  }
  if(right<0)
  {
    right=abs(right);
    digitalWrite(dir_d, LOW);
    analogWrite(pwm_d, right);
  }
  else
  {
    digitalWrite(dir_d, HIGH);
    analogWrite(pwm_d, right);
  }
  if(left==0)
  {
    digitalWrite(dir_e, LOW);
    analogWrite(pwm_e, left);    
  }
  if(right==0)
  {
    digitalWrite(dir_d, LOW);
    analogWrite(pwm_d, right);
  }
}

 
void IniciarMovimento(){     
  long esquerda = DistanciaEsquerda();    
  long frente = DistanciaFrente();    
  long direita = DistanciaDireita();  
  long diff;

// Serial.println(frente);
int kp=7;
  if(frente>40)
  {    
    //caso 1 - centrar no corredor
    //   set_velocidade(180, 180);   
    diff=esquerda-direita;
       if (esquerda <15.0 && direita>18.0 && direita<40.0)
       {
         set_velocidade(225, -170);
       } 
       else if (direita <15.0 && esquerda>18.0 && esquerda<40.0)
       {
         set_velocidade(-170, 225);
       }
       else
       {
         set_velocidade(200, 200);
       }
       
          
    //caso 2 - virar (entrar no quarto)
     if(esquerda>55.0)
     {
      while(esquerda>25)
      {
     
       set_velocidade(200,200);
        delay(250);
       set_velocidade(-180, 255);
       delay(250);
       set_velocidade(200,200);
       delay(100);
       set_velocidade(-190, 255);
       esquerda = DistanciaEsquerda();    
      }
     }
     else if(direita>55.0 && esquerda<20.0)
     {
        set_velocidade(255, -160);
     } 
  }
  else
  {  
   //caso 3 - virar (corredor)
      if(esquerda>30)
      {
        set_velocidade(-100, 255);
      }
      else  if(direita>30)
      {
        set_velocidade(255, -100);
  
   }
  } 
     
    
// while(frente<40){
//      if(flag==0)
//      {
//        set_velocidade(-180, 200);
//        delay(200);
//        set_velocidade(0, 0);
//        espacoEsq=DistanciaFrente(); 
//      
//        set_velocidade(200, -180);
//        delay(400);
//        set_velocidade(0, 0);
//        espacoDir=DistanciaFrente();
//        flag=1;
//      }
//      
//   //caso 4 - Encorralado
//      if(espacoEsq<17 && espacoDir<17)
//      {
//        if((direita+esquerda)>30)
//        {
//          if(direita>esquerda)
//          {
//            set_velocidade(255, -255);
//          }
//          else
//          {
//            set_velocidade(-255,255);
//          }
//        }
//      }
//    
//   //caso 5 - Objeto no corredor
//      if(espacoEsq>17)
//      {
//        set_velocidade(170, (180 + kp*abs(esquerda-direita)));
//      }
//      if(espacoDir>17)
//      {
//        set_velocidade(170, (180 + kp*abs(esquerda-direita)));
//      }
//       direita=DistanciaDireita();
//       esquerda=DistanciaEsquerda();
//       frente=DistanciaFrente(); 
//    } 
//    flag=0;
}

//Função para apagar a chama
void chama() {
 long value = analogRead(sensorValue);
  digitalWrite(PinChama,LOW);
///  delay (500);
  IniciarMovimento();
  while (value >=300 ){
    set_velocidade(0, 0);
    digitalWrite(PinChama,HIGH);
    value = analogRead(sensorValue);
   // delay(500); 
  }
    digitalWrite(PinChama,LOW);
   // set_velocidade(180, 180);
  //  delay(500);
    
    //return sensorValue;
 }


    
void loop()
{   
   IniciarMovimento();
   //int chama5;
   //chama5=chama();
   chama(); 
}
