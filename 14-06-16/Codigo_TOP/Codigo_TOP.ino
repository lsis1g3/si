//Definir Pinos

//Motores:
#define pwm_d  3 //PWM Motor A - Direita
#define pwm_e  11 //PWM Motor B - Esquerda
#define dir_d  12 //DIR Motor A - Direita
#define dir_e  13 //DIR Motor B - Esquerda


//Chama:
#define PinChama  2
#define sensorValue  A4
int ventoinha = 0;
    
//Sonares:
int trigPinF=7;
int echoPinF=6;
int trigPinD=5;
int echoPinD=4;
int trigPinE=8;
int echoPinE=9;

double tempo;



void setup(){    
 //Sonares
   pinMode(trigPinF, OUTPUT);
   pinMode(echoPinF, INPUT); 
   pinMode(trigPinD, OUTPUT);
   pinMode(echoPinD, INPUT);
   pinMode(trigPinE, OUTPUT);
   pinMode(echoPinE, INPUT);
   Serial.begin(9600);   
    
 //Motores
   pinMode(pwm_d, OUTPUT);
   pinMode(pwm_e, OUTPUT);
   pinMode(dir_d, OUTPUT);
   pinMode(dir_e, OUTPUT);
  
 //Chama:
   pinMode (PinChama,OUTPUT);  
}    
    


 
// Função para ler e calcular a distância do sonar  
double lerSonar(int trigPin, int echoPin){
  double distancia;  

digitalWrite(trigPin, LOW);
delayMicroseconds(2);
digitalWrite(trigPin, HIGH);
delayMicroseconds(10);
digitalWrite(trigPin, LOW);
tempo = pulseIn(echoPin, HIGH);
distancia = (tempo/2) / 29.1;
  
  return distancia;                                 // Retorna a distância  
}   

   
// Função para determinar a distância da frente   
double DistanciaFrente(){   
  return (lerSonar(trigPinF,echoPinF));   
}    
    
// Função para determinar a distância da direita    
double DistanciaDireita(){    
  return (lerSonar(trigPinD,echoPinD));   
}    
    
// Função para determinar a distância da esquerda    
double DistanciaEsquerda(){      
  return (lerSonar(trigPinE,echoPinE));   
}    
//Função responsável pelo movimento do robô
//Chama outras funções


 
void set_velocidade(int left, int right)
{
  if(left<0)
  {
    left=abs(left);
    digitalWrite(dir_e, LOW);
    analogWrite(pwm_e, left);    
  }
  else
  {
    digitalWrite(dir_e, HIGH);
    analogWrite(pwm_e, left);    
  }
  if(right<0)
  {
    right=abs(right);
    digitalWrite(dir_d, LOW);
    analogWrite(pwm_d, right);
  }
  else
  {
    digitalWrite(dir_d, HIGH);
    analogWrite(pwm_d, right);
  }
  if(left==0)
  {
    digitalWrite(dir_e, LOW);
    analogWrite(pwm_e, left);    
  }
  if(right==0)
  {
    digitalWrite(dir_d, LOW);
    analogWrite(pwm_d, right);
  }
}

 
void IniciarMovimento(){     
  double esquerda = DistanciaEsquerda();    
  double frente = DistanciaFrente();    
  double direita = DistanciaDireita();  


// Serial.println(frente);

  if(frente>30)
  {    
    //caso 1
    set_velocidade(150, 150);   
    
    if (esquerda <10.0)
    {
      set_velocidade(255, 100);
    } 
    else if (direita <10.0)
    {
      set_velocidade(100, 255);
    }
    
    //caso 2
    if(esquerda>20)
    {
      set_velocidade(-50, 255);
    }
    else
    if(direita>20)
     {
      set_velocidade(255, -50);
    } 
 }     
  else
 {
    //caso 3
    if(esquerda>20)
    {
      set_velocidade(-50, 255);
    }
    else  if(direita>20)
     {
      set_velocidade(255, -50);
    } 
     
    
    //caso 4
    while(frente<30){
       if(direita<25 && esquerda<25)
    {
      if(direita>esquerda)
      {
        set_velocidade(255, -255);
      }
      else
      {
        set_velocidade(-255,255);
      }
    }
    direita=DistanciaDireita();
    esquerda=DistanciaEsquerda();
    frente=DistanciaFrente(); 
    }
 }
     
}
//Função para apagar a chama
int chama() {
 int value = analogRead(sensorValue);
  digitalWrite(PinChama,LOW);
///  delay (500);
  IniciarMovimento();
  if (value >=400 ){
    digitalWrite(PinChama,HIGH);
   // delay(500); 
  }
  else{
    digitalWrite(PinChama,LOW);
  //  delay(500);
    
    return sensorValue;
  }
}

    
void loop()
{   
   IniciarMovimento();
   int chama5;
   
   chama5=chama();
   
}
