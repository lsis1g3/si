int pwm_a = 3; //PWM Motor A
int pwm_b = 11; //PWM Motor B
int dir_a = 12; //DIR Motor A
int dir_b = 13; //DIR Motor B

void setup()
{
pinMode(pwm_a, OUTPUT);
pinMode(pwm_b, OUTPUT);
pinMode(dir_a, OUTPUT);
pinMode(dir_b, OUTPUT);
}

void loop()
{
digitalWrite(dir_a, LOW);
analogWrite(pwm_a, 150);
digitalWrite(dir_b, LOW);
analogWrite(pwm_b, 150);

delay(2000);

digitalWrite(dir_a, LOW);
analogWrite(pwm_a, 150);
digitalWrite(dir_b, HIGH);
analogWrite(pwm_b, 150);

delay(2000);
}
