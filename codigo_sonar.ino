int trigPin = 7;
int echoPin = 6;
double tempo;
double distancia;

void setup() {
 pinMode(trigPin, OUTPUT);
 pinMode(echoPin, INPUT);
 Serial.begin(9600);
}

void loop() {
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  tempo=pulseIn(echoPin, HIGH);
  distancia=(tempo/2)/29.1;
  Serial.println(distancia);
  delay(1000);
}
