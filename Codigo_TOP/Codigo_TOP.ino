
//////////////FALTA INDICAR PINOS DOS SONARES FRENTE; DIR E ESQ, nos metodos de leitura dos sonares
//Definir Pinos

//Sonares:
int trigPin;
int echoPin; 

double tempo;
double distancia;

//Motores:
#define pwm_a  A3; //PWM Motor A
#define pwm_b  A11; //PWM Motor B
#define dir_a  A12; //DIR Motor A
#define dir_b  A13; //DIR Motor B


//Chama:
#define PinChama  A2);
int analogPin;
#define sensorValue  A4);
int ventoinha = 0;
    
 
void setup(){    
 //Sonares
   pinMode(trigPin, OUTPUT);
   pinMode(echoPin, INPUT);
   Serial.begin(9600);  
   
//Motores
   pinMode(pwm_a, OUTPUT);
   pinMode(pwm_b, OUTPUT);
   pinMode(dir_a, OUTPUT);
   pinMode(dir_b, OUTPUT);
   motor1.setSpeed(190);       // Define velocidade do motor 1
   motor2.setSpeed(190);       // Define velocidade do motor 2

//Chama:
   pinMode (PinChama,OUTPUT);
   
}    
    
    
void loop(){    
   IniciarMovimento();  
    
}    
    
//Função responsável pelo movimento do robô
//Chama outras funções
void IniciarMovimento(){     
  int distancia = lerSonar(); // Ler o sonar 
  Serial.print("distancia: "); // Exibe a distância 
  Serial.println(distancia);   
   if (distancia > 25) {    
     andar();    
   }
     else{   
       parar();    
       MovimentoRobo();    
       IniciarMovimento();    
     }   
} 
    
// Função para ler e calcular a distância do sonar  
int lerSonar(int trigPin, int echoPin){    
  digitalWrite(trigPin, LOW);     // Desliga a emisão do som  
  delayMicroseconds(4);            // Aguarda 4 segundos  
  digitalWrite(trigPin, HIGH);     // Liga a trasmisão de som  
  delayMicroseconds(20);            // Continua emitindo o som durante 20 segundos  
  digitalWrite(trigPin, LOW);     // Desliga a emisão do som   
  delayMicroseconds(10);            // Aguarda 10 segundos para poder receber o som  
  long pulse_us = pulseIn(echoPin, HIGH); // Liga o recebedor e calcula quandos pulsos ele recebeu  
  distancia=(tempo/2)/29.1;         // Calcula a distaâcia  
  
  return distancia;             // Retorna a distância  
}   
    
// Função para determinar a distância da frente   
int DistanciaFrente(){   
  int leituraSonar = lerSonar();  // Ler sensor de distância  
  delay(600);   
  leituraSonar = lerSonar();   
  delay(600);   
  Serial.print("Distancia Frente: "); // Exibe no serial  
  Serial.println(leituraSonar);   
  return leituraSonar;       // Retorna a distância  
}    
    
// Função para determinar a distância da direita    
int DistanciaDireita(){    
  int leituraSonar = lerSonar();   
  delay(600);   
  leituraSonar = lerSonar();   
  delay(600);   
  Serial.print("Distancia Direita: ");  
  Serial.println(leituraSonar);   
  return leituraSonar;    
}    
    
// Função para determinar a distância da esquerda    
int DistanciaEsquerda(){      
  int leituraSonar = lerSonar();   
  delay(600);   
  leituraSonar = lerSonar();   
  delay(600);   
  Serial.print("Distancia Esquerda: ");  
  Serial.println(leituraSonar);   
  return leituraSonar;    
}    
    
// Função que calcula qual o lado com maior distância
char MaiorDistancia(){    
  int esquerda = DistanciaEsquerda();    
  int frente = DistanciaFrente();    
  int direita = DistanciaDireita();      
  int maiorDistancia = 0;   
  char melhorDistancia = '0';     
    
  if (frente > direita && frente > esquerda){    
    melhorDistancia = 'f';    
    maiorDistancia = frente;    
  }else   
  if (direita > frente && direita > esquerda){    
    melhorDistancia = 'd';    
    maiorDistancia = direita;    
  }else  
  if (esquerda > frente && esquerda > direita){    
    melhorDistancia = 'e';    
    maiorDistancia = esquerda;    
  }    
    
  return melhorDist;    
}    
    
// Função que decide qual o movimento do carro 
void MovimentoRobo(){    
  char melhorDist = MovimentoRobo();     
  Serial.print("melhor Distancia: ");  
  Serial.println(melhorDist);  
  if (melhorDist == 'c'){    
    andar();    
  }else if (melhorDist == 'd'){    
    rotacao_Direita();    
  }else if (melhorDist == 'e'){    
    rotacao_Esquerda();     
  }   
  }    
     
// Função para fazer o carro parar    
void parar(){    
  Serial.println(" Parar ");  
  motor1.run(RELEASE); // Motor para  
  motor2.run(RELEASE);  
}    
    
// Função para fazer o robô andar para frente    
void andar(){    
  Serial.println(" frente ");   
  motor1.run(FORWARD); 
  motor2.run(FORWARD); 
  delay(500);    
}    

    
// Função que faz o robô virar à direita    
void rotacao_Direita(){    
  Serial.println(" Para a direita ");  
  motor1.run(FORWARD); 
  motor2.run(RELEASE); 
  delay(100);    
}    
    
// Função que faz o robô virar à esquerda    
void rotacao_Esquerda(){    
  Serial.println(" Para a esquerda ");  
  motor1.run(RELEASE); 
  motor2.run(FORWARD); 
  delay(100);    
}

//Função para apagar a chama
int chama() {
  sensorValue = analogRead(A4);
  digitalWrite(PinChama,LOW);
  delay (500);
  
  if (sensorValue >=500 ){
    digitalWrite(PinChama,HIGH);
    delay(500); 
  }
  else{
    digitalWrite(PinChama,LOW);
    delay(500);
    
    return sensorValue;
  }
}


////////////
//void loop(){
//double sonar1, sonar2, sonar3;
//int chama1;
//
//chama1=chama();
//Serial.println ("Valor de chama:");
//Serial.println(chama1);
//
//
//sonar1=sonar(7, 6);
//sonar2=sonar(5, 4);
//sonar3=sonar(9, 8);
//
//Serial.println(sonar1);
//  delay(500);
//Serial.println(sonar2);
//  delay(500);
//  Serial.println(sonar3);
//  delay(500);
//  
//  digitalWrite(dir_a, HIGH);
//analogWrite(pwm_a, 200);
//digitalWrite(dir_b, HIGH);
//analogWrite(pwm_b, 200);
//
//}
