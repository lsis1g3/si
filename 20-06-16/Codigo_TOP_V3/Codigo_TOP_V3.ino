//Definir Pinos

//Motores:
#define pwm_d  3 //PWM Motor A - Direita
#define pwm_e  11 //PWM Motor B - Esquerda
#define dir_d  12 //DIR Motor A - Direita
#define dir_e  13 //DIR Motor B - Esquerda


//Chama:
#define PinChama  2
#define sensorValue  A4
int ventoinha = 0;
    
//Sonares:
int trigPinF=7;
int echoPinF=6;
int trigPinD=5;
int echoPinD=4;
int trigPinE=8;
int echoPinE=9;

long esquerda;    
long frente;    
long direita;  

int flag=0;

int motors=1;

long espacoEsq;
long espacoDir;

void setup(){    
 //Sonares
   pinMode(trigPinF, OUTPUT);
   pinMode(echoPinF, INPUT); 
   pinMode(trigPinD, OUTPUT);
   pinMode(echoPinD, INPUT);
   pinMode(trigPinE, OUTPUT);
   pinMode(echoPinE, INPUT);
   Serial.begin(9600);   
    
 //Motores
   pinMode(pwm_d, OUTPUT);
   pinMode(pwm_e, OUTPUT);
   pinMode(dir_d, OUTPUT);
   pinMode(dir_e, OUTPUT);
  
 //Chama:
   pinMode (PinChama,OUTPUT);  
}    
    


 
// Função para ler e calcular a distância do sonar  
long lerSonar(int trigPin, int echoPin){
  long distancia;  
  long temp;
digitalWrite(trigPin, LOW);
delayMicroseconds(2);
digitalWrite(trigPin, HIGH);
delayMicroseconds(10);
digitalWrite(trigPin, LOW);
temp = pulseIn(echoPin, HIGH);
distancia = (temp/2) / 29.1;
  
return distancia;                                 // Retorna a distância  
}   

   
// Função para determinar a distância da frente   
long DistanciaFrente(){   
  return (lerSonar(trigPinF,echoPinF));   
}    
    
// Função para determinar a distância da direita    
long DistanciaDireita(){    
  return (lerSonar(trigPinD,echoPinD));   
}    
    
// Função para determinar a distância da esquerda    
long DistanciaEsquerda(){      
  return (lerSonar(trigPinE,echoPinE));   
}    
//Função responsável pelo movimento do robô
//Chama outras funções


 
void set_velocidade(int left, int right)
{
  if(motors==1)
  {
  if(left<0)
  {
    left=abs(left);
    digitalWrite(dir_e, LOW);
    analogWrite(pwm_e, left);    
  }
  else
  {
    digitalWrite(dir_e, HIGH);
    analogWrite(pwm_e, left);    
  }
  if(right<0)
  {
    right=abs(right);
    digitalWrite(dir_d, LOW);
    analogWrite(pwm_d, right);
  }
  else
  {
    digitalWrite(dir_d, HIGH);
    analogWrite(pwm_d, right);
  }
  if(left==0)
  {
    digitalWrite(dir_e, LOW);
    analogWrite(pwm_e, left);    
  }
  if(right==0)
  {
    digitalWrite(dir_d, LOW);
    analogWrite(pwm_d, right);
  }
  }
}

 
void IniciarMovimento(){     
  
esquerda = DistanciaEsquerda();    
frente = DistanciaFrente();    
direita = DistanciaDireita();  
  //long diff;

Serial.print("L > ");
Serial.println(esquerda);
Serial.print("R > ");
Serial.println(direita);
Serial.print("F > ");
Serial.println(frente);

//Serial.println(esquerda);
int kp=2;
int L=0, R=0;
  if(frente>40)
  {    
    //caso 1 - centrar no corredor
      
     if (esquerda <32.0 && direita>34.0 && direita<70.0)
     {
      L=100 + abs(esquerda - direita)*kp;
       set_velocidade(L, -100);
     } 
     else if (direita <32.0 && esquerda>34.0 && esquerda<70.0)
     {
       R=100 + abs(esquerda - direita)*kp;
       set_velocidade(-100, R);
     }
     else
     {
       set_velocidade(220, 220);
     }
             
//    //caso 2 - virar (entrar no quarto)
   if(esquerda>82.0)
     {
      while(esquerda>30.0)
      {
       set_velocidade(-180, 255);
       delay(250);
       set_velocidade(220,220);
       delay(200);
       set_velocidade(-190, 255);
       esquerda = DistanciaEsquerda();    
      }
     }
     else if(direita>82.0)
     {
 //     Serial.print("R > ");
  //     Serial.println(direita);
      while(direita>30.0)
      {
    //   Serial.print("While R > ");
     //  Serial.println(direita);
  //     set_velocidade(200,200);
    //   delay(150);
       set_velocidade(255,-180);
       delay(250);
       set_velocidade(200,200);
       delay(250);
       set_velocidade(255, -190);
       direita = DistanciaDireita();
      } 
     }
  }
/*//  //caso 3 - virar (corredor)
//   else
//   {
//    if(esquerda>55.0)
//    {
//      set_velocidade(-190, 255);
//    }
//    else  if(direita>55.0)
//    {
//      set_velocidade(255, -190);
//   }
//  } 
//     
//    
//    
// while(frente<40){
//      if(flag==0)
//      {
//        set_velocidade(-180, 200);
//        delay(200);
//        set_velocidade(0, 0);
//        espacoEsq=DistanciaFrente(); 
//      
//        set_velocidade(200, -180);
//        delay(400);
//        set_velocidade(0, 0);
//        espacoDir=DistanciaFrente();
//        flag=1;
//      }
//      
//   //caso 4 - Encorralado
//      if(espacoEsq<15 && espacoDir<15)
//      {
//        if((direita+esquerda)>30)
//        {
//          if(direita>16)
//          {
//            set_velocidade(255, -255);
//          }
//          else if (esquerda>16)
//          {
//            set_velocidade(-255,255);
//          }
//        }
//      }
    
   //caso 5 - Objeto no corredor
//      if(espacoEsq>17)
//      {
//        set_velocidade(170, (180 + kp*abs(esquerda-direita)));
//      }
//      if(espacoDir>17)
//      {
//        set_velocidade(170, (180 + kp*abs(esquerda-direita)));
//      }
//       direita=DistanciaDireita();
//       esquerda=DistanciaEsquerda();
//       frente=DistanciaFrente(); 
    } 
//    flag=0; */
}


 

//Função para apagar a chama
void chama() {
// int espera=0;
 long value = analogRead(sensorValue);
  digitalWrite(PinChama,LOW);
  while (value >=300  && frente >5){
    set_velocidade(0, 0);
    digitalWrite(PinChama,HIGH);
    delay(500);
    set_velocidade(200,200);
    delay(100);
    value = analogRead(sensorValue);
   // delay(500); 
 //  espera++;
   
  }
    digitalWrite(PinChama,LOW);
 }


    
void loop()
{   
   IniciarMovimento();
   chama(); 
}
